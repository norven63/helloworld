package com.myAndroid.helloworld.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.graphics.drawable.DrawableCompat;

/**
 * @author ZhouXinXing
 * @date 2016年12月12日 20:31
 * @Description
 */

public final class VectorUtil {
    public static Drawable getVector(Context context, int svgId, int colorId) {
        Drawable drawable = VectorDrawableCompat.create(context.getResources(), svgId, context.getTheme());
        Drawable tintIcon = DrawableCompat.wrap(drawable);
        DrawableCompat.setTintList(tintIcon, context.getResources().getColorStateList(colorId));

        return tintIcon;
    }
}
