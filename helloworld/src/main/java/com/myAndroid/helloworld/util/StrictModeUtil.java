package com.myAndroid.helloworld.util;

import android.os.StrictMode;


/**
 * @author ZhouXinXing
 * @date 2016年09月09日 15:50
 * @Description 浅封装StrictMode的接口调用。
 * 网上帖子示例： http://blog.csdn.net/meegomeego/article/details/45746721
 * 官方文档说明： https://developer.android.com/reference/android/os/StrictMode.html
 */
public final class StrictModeUtil {
    private static final boolean DEBUG = false;

    /**
     * 1.开启StrictMode模式。之后观察logcat中，TAG为StrictMode的日志信息。
     * 2.需要在组件的onCreate()方法中调用，且在super.onCreate()之前调用。
     * 3.根据Google官方建议，只有在DEBUG模式下才开启StrictMode模式。这里虽然已经使用AppEnv.bAppdebug进行控制，但还是建议只有在我方人员需要调试性能时才调用该接口。
     */
    public static void openStrictMode() {
        if (DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
    }

    /**
     * 标记可能会耗时的方法，提供给StrictMode检测标识。
     * 需要在该方法的方法体开始处调用。
     *
     * @param methodName 方法名
     */
    public static void noteSlowMethod(String methodName) {
        if (DEBUG) {
            StrictMode.noteSlowCall(methodName);
        }
    }
}
