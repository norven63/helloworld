package com.myAndroid.helloworld.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.myAndroid.helloworld.provider.BatteryDBConfig.BatteryCharge;
import com.myAndroid.helloworld.provider.BatteryDBConfig.BatteryTemperature;
import com.myAndroid.helloworld.provider.BatteryDBConfig.TaskExtendTime;

public class BatteryProvider extends ContentProvider {
    private static final int TASK_EXTENDTIME = 1;
    private static final int BATTERY_TEMPERATURE = 2;
    private static final int BATTERY_CHARGE = 3;

    private BatterySqliteHelper mOpenHelper;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(BatteryDBConfig.AUTHORITY, TaskExtendTime.TABLE_NAME, TASK_EXTENDTIME);
        MATCHER.addURI(BatteryDBConfig.AUTHORITY, BatteryTemperature.TABLE_NAME, BATTERY_TEMPERATURE);
        MATCHER.addURI(BatteryDBConfig.AUTHORITY, BatteryCharge.TABLE_NAME, BATTERY_CHARGE);
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = BatterySqliteHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String groupBy = null;

        int match = MATCHER.match(uri);
        switch (match) {
            case TASK_EXTENDTIME:
                qb.setTables(BatteryDBConfig.TaskExtendTime.TABLE_NAME);
                break;
            case BATTERY_TEMPERATURE:
                qb.setTables(BatteryDBConfig.BatteryTemperature.TABLE_NAME);
                break;
            case BATTERY_CHARGE:
                qb.setTables(BatteryDBConfig.BatteryCharge.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        SQLiteDatabase db;
        try {
            db = mOpenHelper.getReadableDatabase();
        } catch (Exception e) {
            return null;
        }

        Cursor c;
        try {
            c = qb.query(db, projection, selection, selectionArgs, groupBy, null, sortOrder);
        } catch (Exception e) {
            return null;
        }

        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (MATCHER.match(uri)) {
            case TASK_EXTENDTIME:
                return TaskExtendTime.CONTENT_TYPE;
            case BATTERY_TEMPERATURE:
                return BatteryTemperature.CONTENT_TYPE;
            case BATTERY_CHARGE:
                return BatteryCharge.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        Uri ret;
        long rowId;
        int match = MATCHER.match(uri);
        try {
            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            switch (match) {
                case TASK_EXTENDTIME:
                    rowId = db.insert(TaskExtendTime.TABLE_NAME, null, initialValues);
                    break;
                case BATTERY_TEMPERATURE:
                    rowId = db.insert(BatteryTemperature.TABLE_NAME, null, initialValues);
                    break;
                case BATTERY_CHARGE:
                    rowId = db.insert(BatteryCharge.TABLE_NAME, null, initialValues);
                    break;
                default:
                    return null;
            }

            if (rowId > 0) {
                ret = ContentUris.withAppendedId(uri, rowId);

                return ret;
            }
        } catch (Exception ignored) {
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int count = 0;
        try {
            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            switch (MATCHER.match(uri)) {
                case TASK_EXTENDTIME:
                    count = db.delete(TaskExtendTime.TABLE_NAME, where, whereArgs);
                    break;
                case BATTERY_TEMPERATURE:
                    count = db.delete(BatteryTemperature.TABLE_NAME, where, whereArgs);
                    break;
                case BATTERY_CHARGE:
                    count = db.delete(BatteryCharge.TABLE_NAME, where, whereArgs);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
        } catch (Exception ignored) {
        }

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count = 0;
        try {
            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            switch (MATCHER.match(uri)) {
                case TASK_EXTENDTIME:
                    count = db.update(TaskExtendTime.TABLE_NAME, values, where, whereArgs);
                    break;
                case BATTERY_TEMPERATURE:
                    count = db.update(BatteryTemperature.TABLE_NAME, values, where, whereArgs);
                    break;
                case BATTERY_CHARGE:
                    count = db.update(BatteryCharge.TABLE_NAME, values, where, whereArgs);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
        } catch (Exception ignored) {
        }

        return count;
    }
}
