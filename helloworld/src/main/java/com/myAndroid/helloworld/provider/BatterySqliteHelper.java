package com.myAndroid.helloworld.provider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.myAndroid.helloworld.provider.BatteryDBConfig.BatteryCharge;
import com.myAndroid.helloworld.provider.BatteryDBConfig.BatteryTemperature;
import com.myAndroid.helloworld.provider.BatteryDBConfig.TaskExtendTime;

class BatterySqliteHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "battery.db";

    public static final int DB_VERSION = 1;

    private static BatterySqliteHelper dbOpenHelper = null;

    public static BatterySqliteHelper getInstance(Context context) {
        if (dbOpenHelper == null) {
            dbOpenHelper = new BatterySqliteHelper(context, DB_NAME, DB_VERSION);
        }
        return dbOpenHelper;
    }

    private BatterySqliteHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private BatterySqliteHelper(Context context, String name, int version) {
        this(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
    }

    /* 从3.0之后在继承SQLiteOpenHelper的类中，都需要重写该方法（该方法默认抛出异常），否则在数据库降级的时候会抛出异常。 */
    @SuppressLint("Override")
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TaskExtendTime.TABLE_NAME + " (" + TaskExtendTime._ID
                + " INTEGER PRIMARY KEY," + TaskExtendTime.NAME + " TEXT," + TaskExtendTime.RUNTIME + " Long,"
                + TaskExtendTime.EXTENDTIME + " Long," + TaskExtendTime.TYPE + " INTEGER);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + BatteryTemperature.TABLE_NAME + " (" + BatteryTemperature._ID
                + " INTEGER PRIMARY KEY," + BatteryTemperature.TEMPERATURE + " Integer," + BatteryTemperature.TIME
                + " Long);");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + BatteryCharge.TABLE_NAME + " (" + BatteryCharge._ID
                + " INTEGER PRIMARY KEY," + BatteryCharge.STARTTIME + " Long," + BatteryCharge.ENDTIME + " Long,"
                + BatteryCharge.STARTLEVEL + " Integer," + BatteryCharge.ENDLEVEL + " Integer,"
                + BatteryCharge.BOOSTCHARGE_COUNT + " Integer," + BatteryCharge.TEMPERATURECOOL_COUNT + " Integer);");
    }
}
