package com.myAndroid.helloworld.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class BatteryDBConfig {
    public static final String AUTHORITY = "my.provider.demo";

    //TASK触发时的延长时间
    public static final class TaskExtendTime implements BaseColumns {
        public static final String TABLE_NAME = "task_extendtime";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.qihoo.batteryplus.tet";

        private TaskExtendTime() {
        }

        public static final String NAME = "name";
        public static final String RUNTIME = "runtime";
        public static final String EXTENDTIME = "extendtime";
        public static final String TYPE = "type";
        public static final String DEFAULT_SORT_ORDER = RUNTIME + " DESC";

    }

    //温度采样
    public static final class BatteryTemperature implements BaseColumns {
        public static final String TABLE_NAME = "battery_temperature";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.qihoo.batteryplus.bt";

        private BatteryTemperature() {
        }

        public static final String TEMPERATURE = "temperature";
        public static final String TIME = "time";
        public static final String DEFAULT_SORT_ORDER = TIME + " DESC";

    }

    //充电记录
    public static final class BatteryCharge implements BaseColumns {
        public static final String TABLE_NAME = "battery_charge";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.qihoo.batteryplus.bc";

        private BatteryCharge() {
        }

        public static final String STARTTIME = "starttime";
        public static final String ENDTIME = "endtime";
        public static final String STARTLEVEL = "startlevel";
        public static final String ENDLEVEL = "endlevel";
        public static final String BOOSTCHARGE_COUNT = "boostcharge_count";
        public static final String TEMPERATURECOOL_COUNT = "temperaturecool_count";
        public static final String DEFAULT_SORT_ORDER = ENDTIME + " DESC";

    }
}
