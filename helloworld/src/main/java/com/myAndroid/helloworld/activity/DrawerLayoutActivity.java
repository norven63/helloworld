package com.myAndroid.helloworld.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DrawerLayoutActivity extends AppCompatActivity {

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.navigationView)
    NavigationView navigationView;

    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drawer_layout);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                Toast.makeText(DrawerLayoutActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                Toast.makeText(DrawerLayoutActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                String state = "unknow";
                switch (newState) {
                    case ViewDragHelper.STATE_DRAGGING:
                        state = "DRAGGING";//表示正在拖拽拖拉菜单

                        break;
                    case ViewDragHelper.STATE_IDLE:
                        state = "IDLE";//表示已经触发完毕拖拉菜单

                        break;
                    case ViewDragHelper.STATE_SETTLING:
                        state = "SETTLING";//表示刚刚触发拖拉菜单

                        break;
                    default:
                        break;
                }

                Toast.makeText(DrawerLayoutActivity.this, "onDrawerStateChanged -state: " + state, Toast.LENGTH_SHORT)
                        .show();
                super.onDrawerStateChanged(newState);
            }
        };
        actionBarDrawerToggle.syncState();//巨牛逼，直接实现"箭头+三横线"的动画效果！

        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        navigationView.setItemIconTintList(getResources().getColorStateList(R.color.tx_g));//item的icon颜色
        navigationView.setItemTextColor(getResources().getColorStateList(R.color.selector_nav_btn_text));//item的文字颜色
        navigationView.setItemBackgroundResource(R.drawable.selector_nav_btn_bg);//item的背景
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if(item.getTitle().toString().startsWith("nav-a")){
                    /**
                     * 自定义Snackbar
                     */
                    Snackbar snackBar = Snackbar.make(drawerLayout, "自定义的Snackbar", Snackbar.LENGTH_LONG);

                    View snackBarView = snackBar.getView();

                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                    snackBarView.setLayoutParams(layoutParams);

                    snackBarView.setBackgroundColor(0x442196f3);

                    snackBar.show();
                }else{
                    String desc = "点击：" + item.getTitle() + "\n" + item.getItemId();
                    Snackbar.make(drawerLayout, desc, Snackbar.LENGTH_LONG)
                            .setAction("耶耶！不是故意要伤害你的", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(DrawerLayoutActivity.this, "啮齿类世界", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setActionTextColor(0xff2196f3)
                            .show();
                }

                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //根据配置文件渲染菜单项
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

}
