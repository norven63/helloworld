package com.myAndroid.helloworld.activity;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Toast;

import com.myAndroid.helloworld.R;

/**
 * @author ZhouXinXing
 * @date 2016年07月28日 17:19
 * @Description
 */
public class MyActionProvider extends ActionProvider {
    private MenuItem menuItem1;

    private SubMenu subMenu;

    public MyActionProvider(Context context) {
        super(context);
    }

    @Override
    public View onCreateActionView() {
        return null;
    }

    @Override
    public boolean hasSubMenu() {
        return true;
    }


    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        if (this.subMenu != null) {
            return;
        }

        this.subMenu = subMenu;

        menuItem1 = subMenu
                .add("sub item 1")
                .setIcon(R.mipmap.ic_launcher)
                .setActionView(R.layout.layout_custom_action_provider)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(getContext(), "sub item 1点击", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

        subMenu.add("sub item 2").setIcon(R.mipmap.ic_launcher)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        item.setIcon(R.mipmap.ic_notify);
                        item.setTitle("changed!");

                        Toast.makeText(getContext(), "sub item 2点击", Toast.LENGTH_SHORT).show();

                        return true;
                    }
                });
    }
}
