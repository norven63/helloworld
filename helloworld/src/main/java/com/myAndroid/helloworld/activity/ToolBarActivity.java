package com.myAndroid.helloworld.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ToolBarActivity extends AppCompatActivity {
    @Bind(R.id.tool_bar)
    Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool_bar);
        ButterKnife.bind(this);

        initPopWindow();

        toolBar.setNavigationIcon(R.mipmap.ic_notify);//设置导航栏图标
        toolBar.setNavigationContentDescription("Nav");
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ToolBarActivity.this, "Navigation", Toast.LENGTH_SHORT).show();

                finish();
            }
        });

        toolBar.inflateMenu(R.menu.action_bar_menu);//设置右上角的填充菜单
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_refresh:
                        Toast.makeText(ToolBarActivity.this, "bar：刷新", Toast.LENGTH_SHORT).show();

                        break;
                    case R.id.menu_settings:
                        /**
                         * 注意该方法与showAsDropDown()的区别。
                         * showAtLocation()的第二个参数，表示以右上方(0,0)为原点，偏移x、y个坐标量
                         */
                        popupWindow.showAtLocation(toolBar, Gravity.RIGHT | Gravity.TOP, 0, 0);

                        break;
                    case R.id.menu_opt_a:
                        Toast.makeText(ToolBarActivity.this, "bar：opt_a", Toast.LENGTH_SHORT).show();

                        break;
                    case R.id.menu_opt_b:
                        Toast.makeText(ToolBarActivity.this, "bar：opt_b", Toast.LENGTH_SHORT).show();

                        break;
                    default:
                        break;
                }

                return false;
            }
        });
    }

    private PopupWindow popupWindow;

    private void initPopWindow() {
        View popupWindowView = getLayoutInflater().inflate(R.layout.popupmenu, null, false);
        View popBtn = popupWindowView.findViewById(R.id.pop_btn);
        popBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ToolBarActivity.this, "神奇按钮被点击", Toast.LENGTH_SHORT).show();
            }
        });

        popupWindow = new PopupWindow(
                popupWindowView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true);
        popupWindow.setAnimationStyle(R.style.PopWindow);// 出现/退出动画
    }
}
