package com.myAndroid.helloworld.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.common.io.ByteStreams;
import com.myAndroid.helloworld.R;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;


public class GlideActivity extends Activity {
    private final int PERMISSION_CODE_WRITE_EXTERNAL_STORAGE = 123;//申请读写SD卡权限的请求码（读、写两个权限，申请任意其一可得其二，二者属于同一权限组）

    @Bind(R.id.imageView)
    ImageView imageView;
    @Bind(R.id.imageView_2)
    ImageView imageView2;
    @Bind(R.id.imageView_3)
    ImageView imageView3;
    @Bind(R.id.imageView_4)
    ImageView imageView4;
    @Bind(R.id.imageView_5)
    ImageView imageView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_glide);
        ButterKnife.bind(this);

        Glide.with(this)
                .load("http://img3.imgtn.bdimg.com/it/u=533786160,1852532877&fm=21&gp=0.jpg")
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher).crossFade()
                .into(imageView);

        Glide.with(this)
                .load("http://a.hiphotos.baidu.com/image/w%3D310/sign=be191b5ef4246b607b0eb475dbf81a35/b3b7d0a20cf431adcbff06344936acaf2edd9889.jpg")
                .centerCrop()
                .animate(R.anim.test)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView2);

        Glide.with(this)
                .load("http://imgsrc.baidu.com/forum/w%3D580/sign=4129d3843e01213fcf334ed464e636f8/c0e0d5f2b21193137c4718ed62380cd790238d8f.jpg")
                .centerCrop()
                .animate(R.anim.test)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView5);

        //申请WRITE_EXTERNAL_STORAGE权限，Android6.0新增的权限机制
        if (Build.VERSION.SDK_INT >= 23 && !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_CODE_WRITE_EXTERNAL_STORAGE);
        } else {
            //还可以加载物理地址图片
            Glide.with(this)
                    .load(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/gilder_test.jpg")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView3);

            try {
                Glide.with(this)
                        .load(ByteStreams.toByteArray(getAssets().open("glide_test.gif")))
                        .placeholder(R.mipmap.ic_launcher)
                        .into(imageView4);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //以下是在6.0编译基础上，新增的判断权限、请求权限的接口
        //checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);//判断
        //requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE_WRITE_EXTERNAL_STORAGE);//请求
    }


    /**
     * Android6.0新增的权限申请回调函数
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /*
         * 判断权限请求码是否匹配，以及是否获取了所申请的权限。
         * 注意这里的permissions、grantResults两个数组，值都是一一对应的。
         */
        if (requestCode == PERMISSION_CODE_WRITE_EXTERNAL_STORAGE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Glide.with(this)
                    .load(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/gilder_test.jpg")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView3);

            try {
                Glide.with(this)
                        .load(ByteStreams.toByteArray(getAssets().open("glide_test.gif")))
                        .placeholder(R.mipmap.ic_launcher)
                        .into(imageView4);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
