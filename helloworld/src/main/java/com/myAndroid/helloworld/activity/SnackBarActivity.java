package com.myAndroid.helloworld.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SnackBarActivity extends AppCompatActivity {
    @Bind(R.id.action0)
    Button action0;
    @Bind(R.id.activity_snackbar)
    FrameLayout content;
    @Bind(R.id.action1)
    Button action1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbar);
        ButterKnife.bind(this);

        action0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(content, "测试用文案测试用文案测试用文案测试用文案", Snackbar.LENGTH_LONG)
                        .setDuration(Snackbar.LENGTH_INDEFINITE)//Snackbar.LENGTH_INDEFINITE 可以实现不消失，只在点击Action才消失的效果
                        .setActionTextColor(0xff2196f3)
                        .setAction("耶耶！不是故意要伤害你的", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(SnackBarActivity.this, "啮齿类世界", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setCallback(new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);

                                String eventStr = "";
                                switch (event) {
                                    case Snackbar.Callback.DISMISS_EVENT_SWIPE:
                                        eventStr = "SWIPE 滑动消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_ACTION:
                                        eventStr = "ACTION 点击Action消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_TIMEOUT:
                                        eventStr = "TIMEOUT 超时消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_MANUAL:
                                        eventStr = "MANUAL 调用dismiss()方法";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE:
                                        eventStr = "CONSECUTIVE 一个新的SnackBar出现";
                                        break;
                                }

                                Toast.makeText(SnackBarActivity.this, eventStr, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onShown(Snackbar snackbar) {
                                super.onShown(snackbar);

                                Toast.makeText(SnackBarActivity.this, "onShown()", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

        action1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * 自定义Snackbar
                 */
                Snackbar snackBar = Snackbar.make(content, "自定义的Snackbar", Snackbar.LENGTH_LONG);

                View snackBarView = snackBar.getView();

                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                snackBarView.setLayoutParams(layoutParams);//定义宽高、出现的位置

                snackBarView.setBackgroundColor(0x442196f3);//定义背景

                TextView snackBarTextView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
                snackBarTextView.setTextColor(0xFF2196f3);//定义文字颜色

                snackBar.show();
            }
        });
    }
}
