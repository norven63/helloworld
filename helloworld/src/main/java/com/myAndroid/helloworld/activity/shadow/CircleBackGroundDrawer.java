package com.myAndroid.helloworld.activity.shadow;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;

/**
 * @author ZhouXinXing
 * @date 2016年06月13日 15:28
 * @Description 圆形背景绘制器
 */
public class CircleBackGroundDrawer implements IBgDrawer {
    private float diameter = 0;//圆形直径
    private float cx = 0;//圆心x坐标
    private float cy = 0;//圆心y坐标

    public CircleBackGroundDrawer(float sdDx, float sdDy, float sdRadius, int viewWidth, int viewHeight) {
        //这里计算圆形直径时减去sdRadius * 2，是因为sdRadius含义为上、下、左、右的阴影宽度大小。所以若要计算圆的直径，则需要整个视图的高（或宽，参照较小值）同时减去上下或者左右两边的阴影宽度，即sdRadius * 2
        diameter = viewWidth <= viewHeight ? (viewWidth - sdRadius * 2) : (viewHeight - sdRadius * 2);

        //圆心坐标 = 视图中心坐标 - 阴影偏移坐标。如果不这样计算，圆形+阴影整体将不会居中，且阴影有可能会溢出于视图外。
        cx = viewWidth / 2 - sdDx;
        cy = viewHeight / 2 - sdDy;
    }

    @Override
    public void drawBg(Canvas canvas, BackGroundPalette backGroundPalette, Paint paint) {
        if (backGroundPalette.isShader()) {
            LinearGradient lg = new LinearGradient(
                    cx - diameter / 2,
                    cy - diameter / 2,
                    cx - diameter / 2,
                    cy + diameter / 2,
                    backGroundPalette.getStartColor(),
                    backGroundPalette.getEndColor(),
                    Shader.TileMode.CLAMP);

            paint.setShader(lg);
        } else {
            paint.setColor(backGroundPalette.getBgColor());
        }

        canvas.drawCircle(cx, cy, diameter / 2, paint);
    }

    @Override
    public float[] getBgCenter() {
        return new float[]{cx, cy};
    }

    @Override
    public int getMaxSrcLength() {
        return (int) (Math.sqrt(2) * diameter / 2);//返回圆的内接正方形边长，公式为：半径*根号2
    }
}
