package com.myAndroid.helloworld.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StatusBarActivity2 extends AppCompatActivity {
    @Bind(R.id.SYSTEM_UI_FLAG_VISIBLE)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_bar2);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.SYSTEM_UI_FLAG_VISIBLE)
    public void onClick1() {
        button.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
    }

    @OnClick(R.id.INVISIBLE)
    public void onClick2() {
        button.setSystemUiVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.SYSTEM_UI_FLAG_FULLSCREEN)
    public void onClick3() {
        button.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @OnClick(R.id.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    public void onClick4() {
        button.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @OnClick(R.id.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    public void onClick5() {
        button.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @OnClick(R.id.button_6)
    public void onClick6() {
        ActionBar actionBar = getSupportActionBar();
        //        actionBar.setBackgroundDrawable(null);//透明Action Bar
        actionBar.hide();//隐藏状态栏

        //透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
