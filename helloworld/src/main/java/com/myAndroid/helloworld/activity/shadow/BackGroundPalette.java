package com.myAndroid.helloworld.activity.shadow;

/**
 * @author ZhouXinXing
 * @date 2016年06月13日 15:30
 * @Description 背景调色盘
 */
public class BackGroundPalette {
    private final boolean shader;//是否需要渐变色
    private final int startColor;//渐变起始色
    private final int endColor;//渐变结束色
    private int bgColor;//纯色背景色
    private final float cornerRadius;//矩形背景的圆角

    public BackGroundPalette(boolean shader, int startColor, int endColor, int bgColor, float cornerRadius) {
        this.shader = shader;
        this.startColor = startColor;
        this.endColor = endColor;
        this.bgColor = bgColor;
        this.cornerRadius = cornerRadius;
    }

    public boolean isShader() {
        return shader;
    }

    public int getStartColor() {
        return startColor;
    }

    public int getEndColor() {
        return endColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getBgColor() {
        return bgColor;
    }

    public float getCornerRadius() {
        return cornerRadius;
    }
}
