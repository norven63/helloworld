package com.myAndroid.helloworld.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.widget.ImageView;

public class PorterDuffActivity extends Activity {

    /**
     1.PorterDuff.Mode.CLEAR
     所绘制不会提交到画布上。

     2.PorterDuff.Mode.SRC
     显示上层绘制图片

     3.PorterDuff.Mode.DST
     显示下层绘制图片

     4.PorterDuff.Mode.SRC_OVER
     正常绘制显示，上下层绘制叠盖。

     5.PorterDuff.Mode.DST_OVER
     上下层都显示。下层居上显示。

     6.PorterDuff.Mode.SRC_IN
     取两层绘制交集。显示上层。

     7.PorterDuff.Mode.DST_IN
     取两层绘制交集。显示下层。

     8.PorterDuff.Mode.SRC_OUT
     取上层绘制非交集部分。

     9.PorterDuff.Mode.DST_OUT
     取下层绘制非交集部分。

     10.PorterDuff.Mode.SRC_ATOP
     取下层非交集部分与上层交集部分

     11.PorterDuff.Mode.DST_ATOP
     取上层非交集部分与下层交集部分

     12.PorterDuff.Mode.XOR
     取两图层全部区域，去除两图层交集部分（会露出底色）

     13.PorterDuff.Mode.DARKEN
     取两图层全部区域，交集部分颜色加深

     14.PorterDuff.Mode.LIGHTEN
     取两图层全部，点亮交集部分颜色

     15.PorterDuff.Mode.MULTIPLY
     取两图层交集部分叠加后颜色

     16.PorterDuff.Mode.SCREEN（请示用XOR）
     取两图层全部区域，交集部分变为透明色
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageView contentView = new ImageView(this);
        Bitmap contentBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(contentBitmap);
        Paint paint = new Paint();
        paint.setTextSize(50);

        paint.setColor(Color.GREEN);
        RectF rectF = new RectF(0, 203, 260, 500);
        canvas.drawRect(rectF, paint);

        paint.setColor(Color.BLUE);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawText("神", 220, 220, paint);

        paint.setXfermode(null);

        contentView.setImageBitmap(contentBitmap);
        setContentView(contentView);
    }
}
