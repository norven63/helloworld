package com.myAndroid.helloworld.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import com.myAndroid.helloworld.R;
import com.myAndroid.helloworld.activity.shadow.ShadowView;

public class ShadowActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shadow);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ShadowView shadowView = (ShadowView) findViewById(R.id.shadow_test);
                shadowView.setBgColor(0xffcc0000);//切记需要8位数的色值
            }
        }, 2000);
    }
}
