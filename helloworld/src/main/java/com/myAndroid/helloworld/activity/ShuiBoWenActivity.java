package com.myAndroid.helloworld.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShuiBoWenActivity extends Activity {

    @Bind(R.id.shui_bo_wen_view)
    View shuiBoWenView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shui_bo_wen);
        ButterKnife.bind(this);

        Animation rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_shui_bo_wen_translate);
        shuiBoWenView.startAnimation(rotateAnimation);
    }
}
