package com.myAndroid.helloworld.activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.myAndroid.helloworld.R;
import com.myAndroid.helloworld.activity.recyclerView.BaseRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CoordinatorLayoutActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @Bind(R.id.main_content)
    CoordinatorLayout mainContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinator_layout);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        List<String> dataSource = new ArrayList<>();
        for (int i = 0; i < 35; i++) {
            dataSource.add("test" + i);
        }
        CoordinatorLayoutAdapter adapter = new CoordinatorLayoutAdapter(dataSource);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        /*
         * 设置完全展开时的title字色。
         * 在展开、收缩时，该设置色会与TitleBar中的title自色有一个过度动画效果
         * 这里设置了00开头，是为了希望在最终展开时，能完全隐藏掉title
         */
        collapsingToolbar.setExpandedTitleColor(0x00000000);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(mainContent, "测试用文案测试用文案测试用文案测试用文案", Snackbar.LENGTH_LONG)
                        .setDuration(Snackbar.LENGTH_INDEFINITE)//Snackbar.LENGTH_INDEFINITE 可以实现不消失，只在点击Action才消失的效果
                        .setActionTextColor(0xff2196f3)
                        .setAction("耶耶！不是故意要伤害你的", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(CoordinatorLayoutActivity.this, "啮齿类世界", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setCallback(new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);

                                String eventStr = "";
                                switch (event) {
                                    case Snackbar.Callback.DISMISS_EVENT_SWIPE:
                                        eventStr = "SWIPE 滑动消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_ACTION:
                                        eventStr = "ACTION 点击Action消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_TIMEOUT:
                                        eventStr = "TIMEOUT 超时消失";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_MANUAL:
                                        eventStr = "MANUAL 调用dismiss()方法";
                                        break;
                                    case Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE:
                                        eventStr = "CONSECUTIVE 一个新的SnackBar出现";
                                        break;
                                }

                                Toast.makeText(CoordinatorLayoutActivity.this, eventStr, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onShown(Snackbar snackbar) {
                                super.onShown(snackbar);

                                Toast.makeText(CoordinatorLayoutActivity.this, "onShown()", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });
    }

    public class CoordinatorLayoutAdapter extends BaseRecyclerViewAdapter<String, CoordinatorLayoutAdapter.CoordinatorLayoutViewHolder> {
        public class CoordinatorLayoutViewHolder extends RecyclerView.ViewHolder {
            public CoordinatorLayoutViewHolder(View itemView) {
                super(itemView);
            }
        }

        public CoordinatorLayoutAdapter(List<String> dataSource) {
            super(dataSource);
        }

        @Override
        public CoordinatorLayoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CoordinatorLayoutViewHolder(new TextView(CoordinatorLayoutActivity.this));
        }

        @Override
        public void onBindViewHolder(CoordinatorLayoutViewHolder holder, int position) {
            TextView textView = (TextView) holder.itemView;

            textView.setText(getItem(position));
        }
    }
}
