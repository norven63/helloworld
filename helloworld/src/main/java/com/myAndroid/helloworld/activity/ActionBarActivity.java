package com.myAndroid.helloworld.activity;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.myAndroid.helloworld.R;

public class ActionBarActivity extends AppCompatActivity {
    private PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_bar);

        /**
         * 记住这里一定要用getSupportActionBar()，否则如果用传统的getActionBar()会返回null
         */
        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);//设置是否需要显示返回键
        actionBar.setTitle("主标题");
        actionBar.setSubtitle("副标题");

        /**
         * 显示自定义布局，替代标题
         */
        actionBar.setDisplayShowCustomEnabled(true);//显示自定义视图
        actionBar.setDisplayShowTitleEnabled(false);//关闭标题栏
        View customMenuView = getLayoutInflater().inflate(R.layout.layout_custom_action_bar, null);
        actionBar.setCustomView(customMenuView);

        initPopWindow();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //根据配置文件渲染菜单项
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);

        setCustomMenuItemView(menu, R.id.menu_settings);

        return super.onCreateOptionsMenu(menu);
    }


    /**
     * 自定义menu item图标布局
     */
    private void setCustomMenuItemView(Menu menu, int menuItemId) {
        final MenuItem menuItem = menu.findItem(menuItemId);
        MenuItemCompat.setActionView(menuItem, R.layout.layout_custom_menu_item);
        View customButton = MenuItemCompat.getActionView(menuItem);
        ImageView customMenuIcon = (ImageView) customButton.findViewById(R.id.custom_menu_icn);
        customMenuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View paramView) {
                onOptionsItemSelected(menuItem);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*
         * 菜单项点击事件
         */
        switch (item.getItemId()) {
            case android.R.id.home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();

                finish();
                break;
//            case R.id.menu_refresh:
//                Toast.makeText(this, "bar：刷新", Toast.LENGTH_SHORT).show();
//
//                break;
            case R.id.menu_settings:
                popupWindow.showAsDropDown(item.getActionView());//显示自定义浮窗

                break;
            case R.id.menu_opt_a:
                Toast.makeText(this, "bar：opt_a", Toast.LENGTH_SHORT).show();

                break;
            case R.id.menu_opt_b:
                Toast.makeText(this, "bar：opt_b", Toast.LENGTH_SHORT).show();

                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initPopWindow() {
        View popupWindowView = getLayoutInflater().inflate(R.layout.popupmenu, null, false);
        View popBtn = popupWindowView.findViewById(R.id.pop_btn);
        popBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActionBarActivity.this, "神奇按钮被点击", Toast.LENGTH_SHORT).show();
            }
        });

        popupWindow = new PopupWindow(
                popupWindowView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true);
        popupWindow.setAnimationStyle(android.R.style.Animation_Translucent);// 出现/退出动画
    }
}
