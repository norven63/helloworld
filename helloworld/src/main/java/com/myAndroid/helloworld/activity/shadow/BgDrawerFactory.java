package com.myAndroid.helloworld.activity.shadow;

/**
 * @author ZhouXinXing
 * @date 2016年06月13日 15:54
 * @Description 背景绘制器工厂
 */
public final class BgDrawerFactory {
    public static IBgDrawer createBgDrawer(ShadowView shadowView) {
        IBgDrawer bgDrawer = null;

        if (shadowView.bgType == ShadowView.BG_TYPE_RECT) {
            bgDrawer = new RectBackGroundDrawer(shadowView.sdDx, shadowView.sdDy, shadowView.sdRadius, shadowView.getWidth(), shadowView.getHeight());
        } else if (shadowView.bgType == ShadowView.BG_TYPE_CIRCLE) {
            bgDrawer = new CircleBackGroundDrawer(shadowView.sdDx, shadowView.sdDy, shadowView.sdRadius, shadowView.getWidth(), shadowView.getHeight());
        }

        return bgDrawer;
    }
}

