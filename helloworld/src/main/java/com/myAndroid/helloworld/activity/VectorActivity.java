package com.myAndroid.helloworld.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.widget.ImageView;

import com.myAndroid.helloworld.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VectorActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Bind(R.id.vector)
    ImageView vector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_vector);
        ButterKnife.bind(this);

        Drawable icon = vector.getDrawable();
        Drawable tintIcon = DrawableCompat.wrap(icon);
        DrawableCompat.setTintList(tintIcon, getResources().getColorStateList(android.R.color.holo_blue_bright));
        vector.setImageDrawable(tintIcon);

//        final ImageView imageView = (ImageView) findViewById(R.id.image_view);
//        if (imageView == null) {
//            return;
//        }
//
//        Drawable drawable = imageView.getDrawable();
//        if (drawable instanceof Animatable) {
//            ((Animatable) drawable).start();
//        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                AnimatedVectorDrawableCompat animatedVectorDrawableCompat = AnimatedVectorDrawableCompat.create(
//                        VectorActivity.this,
//                        R.drawable.anim_vector_test_2);
//
//                if (animatedVectorDrawableCompat == null) {
//                    return;
//                }
//
//                imageView.setImageDrawable(animatedVectorDrawableCompat);
//                animatedVectorDrawableCompat.start();
//            }
//        }, 1200);
    }
}
