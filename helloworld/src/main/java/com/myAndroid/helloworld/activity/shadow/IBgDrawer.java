package com.myAndroid.helloworld.activity.shadow;

/**
 * @author ZhouXinXing
 * @date 2016年06月13日 15:28
 * @Description 背景绘制器接口
 */

import android.graphics.Canvas;
import android.graphics.Paint;

public interface IBgDrawer {
    /**
     * 绘制背景
     *
     * @param canvas     画布
     * @param paint      画笔
     * @param backGroundPalette 渐变色抽象
     */
    void drawBg(Canvas canvas, BackGroundPalette backGroundPalette, Paint paint);

    /**
     * 获取背景中心坐标
     *
     * @return float[x, y]
     */
    float[] getBgCenter();

    /**
     * 获取资源图最大支持长度
     */
    int getMaxSrcLength();
}
