package com.myAndroid.helloworld.activity.shadow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.myAndroid.helloworld.R;

/**
 * @author ZhouXinXing
 * @date 2016年03月11日 20:12
 * @Description 附带阴影的视图
 */
public class ShadowView extends View {
    public static final int BG_TYPE_RECT = -1;
    public static final int BG_TYPE_CIRCLE = -2;
    public static final int NO_SRC = -3;

    int sdColor = 0x00ffffff;//阴影颜色
    float sdDx = 0;//阴影x轴偏移量
    float sdDy = 0;//阴影轴偏移量
    float sdRadius = 0;//阴影柔边（即上、下、左、右露出阴影部分的宽度大小）
    int bgType = -1;//背景形状
    int src = NO_SRC;//资源图id

    private BackGroundPalette backGroundPalette;//背景颜色
    private Paint bgPaintWithShadow;//背景画笔
    private Paint srcPaint;//资源图画笔
    private Paint textPaint;//文本画笔
    private String text = "";//文本

    public ShadowView(Context context) {
        super(context);

        init(null);
    }

    public ShadowView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public ShadowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    private void init(AttributeSet attrs) {
        boolean shader = false;
        int startColor = 0x00ffffff;
        int endColor = 0x00ffffff;
        int bgColor = 0x00ffffff;
        int textColor = 0x00ffffff;
        float textSize = 0f;
        float cornerRadius = 0f;

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.shadowView);

            shader = typedArray.getBoolean(R.styleable.shadowView_shader, false);
            startColor = typedArray.getColor(R.styleable.shadowView_start_color, startColor);
            endColor = typedArray.getColor(R.styleable.shadowView_end_color, endColor);
            bgColor = typedArray.getColor(R.styleable.shadowView_bg_color, bgColor);
            cornerRadius = typedArray.getDimension(R.styleable.shadowView_corner_radius, cornerRadius);

            sdColor = typedArray.getColor(R.styleable.shadowView_sd_color, sdColor);
            sdDx = typedArray.getDimension(R.styleable.shadowView_sd_dx, sdDx);
            sdDy = typedArray.getDimension(R.styleable.shadowView_sd_dy, sdDy);
            sdRadius = typedArray.getDimension(R.styleable.shadowView_sd_radius, sdRadius);
            bgType = typedArray.getInteger(R.styleable.shadowView_bg_type, bgType);
            src = typedArray.getResourceId(R.styleable.shadowView_src, NO_SRC);

            text = typedArray.getString(R.styleable.shadowView_text);
            textColor = typedArray.getColor(R.styleable.shadowView_text_color, textColor);
            textSize = typedArray.getDimension(R.styleable.shadowView_text_size, textSize);

            typedArray.recycle();
        }

        setLayerType(LAYER_TYPE_SOFTWARE, null);//关闭硬件加速。此方法一定要调用，否则阴影无效。

        backGroundPalette = new BackGroundPalette(shader, startColor, endColor, bgColor, cornerRadius);

        srcPaint = new Paint();

        textPaint = new Paint();
        textPaint.setTextSize(textSize);
        textPaint.setColor(textColor);
        textPaint.setTextAlign(Paint.Align.CENTER);

        bgPaintWithShadow = new Paint();
        bgPaintWithShadow.setAntiAlias(true);//设置反锯齿
        bgPaintWithShadow.setFilterBitmap(true);
        /**
         * 设定阴影(柔边, X轴偏移, Y轴偏移, 阴影颜色)
         * x、y只是控制阴影的位置偏移，控制阴影宽度大小的是柔边。
         */
        bgPaintWithShadow.setShadowLayer(sdRadius, sdDx, sdDy, sdColor);
    }

    public void setBgColor(@ColorInt int color) {
        backGroundPalette.setBgColor(color);

        invalidate();
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        IBgDrawer bgDrawer = BgDrawerFactory.createBgDrawer(this);

        if (bgDrawer == null) {
            return;
        }

        //绘制背景
        bgDrawer.drawBg(canvas, backGroundPalette, bgPaintWithShadow);

        float[] bgCenter = bgDrawer.getBgCenter();
        float bgCenterX = bgCenter[0];
        float bgCenterY = bgCenter[1];

        /**
         * 绘制图片资源。以背景的中心点为轴心。
         */
        Bitmap srcBitmap = src == NO_SRC ? null : BitmapFactory.decodeResource(getResources(), src);
        if (srcBitmap != null) {
            /*
             * 如果图片资源边长大小超出了支持的最大长度，需要缩放处理
             */
            int maxSrcLength = srcBitmap.getWidth() > srcBitmap.getHeight() ? srcBitmap.getWidth() : srcBitmap.getHeight();//计算图片资源较长的一条边
            if (maxSrcLength >= bgDrawer.getMaxSrcLength()) {
                float scale = (float) bgDrawer.getMaxSrcLength() / (float) maxSrcLength;

                Matrix matrix = new Matrix();
                matrix.postScale(scale, scale);

                srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
            }

            /*
             * 计算图片资源的左边距、顶边距
             */
            float bitmapLeft = bgCenterX - srcBitmap.getWidth() / 2;
            float bitmapTop = bgCenterY - srcBitmap.getHeight() / 2;

            canvas.drawBitmap(srcBitmap, bitmapLeft, bitmapTop, srcPaint);
        }

        if (TextUtils.isEmpty(text)) {
            return;
        }

        /**
         * 绘制文本
         * 为达到文字垂直居中的效果，需要先计算baseline的y坐标，参阅 http://blog.csdn.net/carrey1989/article/details/10399727
         */
        Paint.FontMetrics fm = textPaint.getFontMetrics();
        float baseLineY = bgCenterY - fm.descent + (fm.bottom - fm.top) / 2;

        canvas.drawText(text, bgCenterX, baseLineY, textPaint);
    }
}
