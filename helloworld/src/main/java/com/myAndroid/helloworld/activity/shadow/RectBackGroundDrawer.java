package com.myAndroid.helloworld.activity.shadow;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * @author ZhouXinXing
 * @date 2016年06月13日 15:28
 * @Description 矩形背景绘制器
 */
public class RectBackGroundDrawer implements IBgDrawer {
    private final float left;
    private final float top;
    private final float right;
    private final float bottom;
    private float rectWidth;
    private float rectHeight;

    public RectBackGroundDrawer(float sdDx, float sdDy, float sdRadius, int viewWidth, int viewHeight) {
        /**
         * 须知：如果radius大于dx或者dy，则可能会出现阴影益处的现象。因为计算得出的left、top为正值，导致绘制的矩形不会顶边。
         */
        left = sdRadius - sdDx; //阴影宽度 - 阴影x轴偏移 = 矩形绘制的最左坐标
        top = sdRadius - sdDy; //阴影宽度 - 阴影y轴偏移 = 矩形绘制的最顶坐标
        right = viewWidth - sdRadius - sdDx; //视图宽度 - 阴影宽度 - 阴影x轴偏移 = 矩形绘制的最右坐标
        bottom = viewHeight - sdRadius - sdDy; //视图高度 - 阴影宽度 - 阴影y轴偏移 = 矩形绘制的最底坐标

        rectWidth = right - left;//矩形背景宽度
        rectHeight = bottom - top;//矩形背景高度
    }

    @Override
    public void drawBg(Canvas canvas, BackGroundPalette backGroundPalette, Paint paint) {
        if (backGroundPalette.isShader()) {
            //由于这里实现的渐变效果是自上而下的垂直渐变，所以x坐标重合，仅y坐标变化
            LinearGradient lg = new LinearGradient(
                    left,
                    top,
                    left,
                    bottom,
                    backGroundPalette.getStartColor(),
                    backGroundPalette.getEndColor(),
                    Shader.TileMode.CLAMP);

            paint.setShader(lg);
        } else {
            paint.setColor(backGroundPalette.getBgColor());
        }

        RectF rectF = new RectF(left, top, right, bottom);
        canvas.drawRoundRect(rectF, backGroundPalette.getCornerRadius(), backGroundPalette.getCornerRadius(), paint);
    }

    @Override
    public float[] getBgCenter() {
        float bgCenterX = left + rectWidth / 2;//背景中心x坐标
        float bgCenterY = top + rectHeight / 2;//背景中心y坐标

        return new float[]{bgCenterX, bgCenterY};
    }

    @Override
    public int getMaxSrcLength() {
        return rectWidth < rectHeight ? (int) rectWidth : (int) rectHeight;
    }
}
