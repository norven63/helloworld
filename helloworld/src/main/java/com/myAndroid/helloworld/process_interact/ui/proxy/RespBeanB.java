package com.myAndroid.helloworld.process_interact.ui.proxy;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhouXinXing
 * @date 2016年07月06日 11:10
 * @Description
 */
public class RespBeanB implements Parcelable {
    private final int age;
    private final String name;
    private final List<RespBeanA> respBeanAs;

    public RespBeanB(int age, String name) {
        this.age = age;
        this.name = name;

        respBeanAs = new ArrayList<>();
        respBeanAs.add(new RespBeanA("aaaa"));
        respBeanAs.add(new RespBeanA("bbbb"));
    }

    protected RespBeanB(Parcel in) {
        age = in.readInt();
        name = in.readString();
        respBeanAs = in.createTypedArrayList(RespBeanA.CREATOR);
    }

    public static final Creator<RespBeanB> CREATOR = new Creator<RespBeanB>() {
        @Override
        public RespBeanB createFromParcel(Parcel in) {
            return new RespBeanB(in);
        }

        @Override
        public RespBeanB[] newArray(int size) {
            return new RespBeanB[size];
        }
    };

    public List<RespBeanA> getRespBeanA() {
        return respBeanAs;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(age);
        dest.writeString(name);
        dest.writeTypedList(respBeanAs);
    }
}
