package com.myAndroid.helloworld.process_interact.core;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;

public abstract class BaseProcessInteractProvider extends ContentProvider {
    public BaseProcessInteractProvider() {
    }

    @Nullable
    @Override
    public Bundle call(String method, String proxyClassName, Bundle extras) {
        Bundle respBundle = new Bundle();

        try {
            extras.setClassLoader(Class.forName(proxyClassName).getClassLoader());

            IProcessInteractProxy processInteractProxy = extras.getParcelable(ProcessInteractUtil.PROXY);
            if (processInteractProxy == null) {
                throw new IllegalArgumentException("无效的进程通讯代理对象");
            }

            Parcelable respBean = processInteractProxy.interact();
            respBundle.putParcelable(BaseProcessInteractProxy.KEY_RESP_BEAN, respBean);
        } catch (Exception ignored) {

        }

        return respBundle;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
