package com.myAndroid.helloworld.process_interact.ui;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * 测试用服务，在process_interact进程中修改测试单例的成员变量
 */
public class ProcessInteractService extends Service {
    public ProcessInteractService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TestSingleInstance.getInstance.init(123, "Norven");
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
