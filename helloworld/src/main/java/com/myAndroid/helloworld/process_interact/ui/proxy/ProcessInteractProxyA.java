package com.myAndroid.helloworld.process_interact.ui.proxy;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.myAndroid.helloworld.process_interact.core.BaseProcessInteractProxy;
import com.myAndroid.helloworld.process_interact.ui.provider.ProcessInteractProviderA;
import com.myAndroid.helloworld.process_interact.ui.TestSingleInstance;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 18:34
 * @Description
 */
public class ProcessInteractProxyA extends BaseProcessInteractProxy<RespBeanA> {
    private final int age;
    private final String name;

    public ProcessInteractProxyA(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public RespBeanA interact() {
        RespBeanA respBeanA = new RespBeanA("age:" + TestSingleInstance.getInstance.getAge() + " - " + age + "; name:" + TestSingleInstance.getInstance.getName() + " - " + name);

        return respBeanA;
    }

    @Override
    public Uri getUri() {
        return ProcessInteractProviderA.URI;
    }

    @Override
    public ClassLoader getRespBeanClassLoader() {
        return RespBeanA.class.getClassLoader();
    }

    protected ProcessInteractProxyA(Parcel in) {
        age = in.readInt();
        name = in.readString();
    }

    public static final Parcelable.Creator<ProcessInteractProxyA> CREATOR = new Parcelable.Creator<ProcessInteractProxyA>() {
        @Override
        public ProcessInteractProxyA createFromParcel(Parcel in) {
            return new ProcessInteractProxyA(in);
        }

        @Override
        public ProcessInteractProxyA[] newArray(int size) {
            return new ProcessInteractProxyA[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(age);
        parcel.writeString(name);
    }
}
