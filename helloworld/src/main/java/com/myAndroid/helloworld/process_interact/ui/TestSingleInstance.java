package com.myAndroid.helloworld.process_interact.ui;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 19:09
 * @Description 测试用单例
 */
public enum TestSingleInstance {
    getInstance;

    private int age = 0;
    private String name = "default";

    public void init(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
}
