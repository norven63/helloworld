package com.myAndroid.helloworld.process_interact.ui.proxy;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.myAndroid.helloworld.process_interact.core.BaseProcessInteractProxy;
import com.myAndroid.helloworld.process_interact.ui.provider.ProcessInteractProviderB;
import com.myAndroid.helloworld.process_interact.ui.TestSingleInstance;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 18:34
 * @Description
 */
public class ProcessInteractProxyB extends BaseProcessInteractProxy<RespBeanB> {
    private final int age;
    private final String name;

    public ProcessInteractProxyB(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public RespBeanB interact() {
        RespBeanB respBeanB = new RespBeanB(TestSingleInstance.getInstance.getAge(), TestSingleInstance.getInstance.getName() + " - " + name);

        return respBeanB;
    }

    @Override
    public Uri getUri() {
        return ProcessInteractProviderB.URI;
    }

    @Override
    public ClassLoader getRespBeanClassLoader() {
        return RespBeanB.class.getClassLoader();
    }

    protected ProcessInteractProxyB(Parcel in) {
        age = in.readInt();
        name = in.readString();
    }

    public static final Parcelable.Creator<ProcessInteractProxyB> CREATOR = new Parcelable.Creator<ProcessInteractProxyB>() {
        @Override
        public ProcessInteractProxyB createFromParcel(Parcel in) {
            return new ProcessInteractProxyB(in);
        }

        @Override
        public ProcessInteractProxyB[] newArray(int size) {
            return new ProcessInteractProxyB[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(age);
        parcel.writeString(name);
    }
}
