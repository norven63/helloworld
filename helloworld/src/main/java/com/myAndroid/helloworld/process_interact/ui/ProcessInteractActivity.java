package com.myAndroid.helloworld.process_interact.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.myAndroid.helloworld.R;
import com.myAndroid.helloworld.process_interact.core.ProcessInteractUtil;
import com.myAndroid.helloworld.process_interact.ui.proxy.ProcessInteractProxyA;
import com.myAndroid.helloworld.process_interact.ui.proxy.ProcessInteractProxyB;
import com.myAndroid.helloworld.process_interact.ui.proxy.RespBeanA;
import com.myAndroid.helloworld.process_interact.ui.proxy.RespBeanB;

public class ProcessInteractActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_interact);

        startService(new Intent(this, ProcessInteractService.class));

        final TextView textViewA = (TextView) findViewById(R.id.contentA);
        textViewA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * 进程通讯A
                 */
                RespBeanA respBeanA = ProcessInteractUtil.interact(ProcessInteractActivity.this, new ProcessInteractProxyA(9, "通讯A"));
                textViewA.setText(respBeanA.getRespString());
            }
        });

        final TextView textViewB = (TextView) findViewById(R.id.contentB);
        textViewB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * 进程通讯B
                 */
                RespBeanB respBeanB = ProcessInteractUtil.interact(ProcessInteractActivity.this, new ProcessInteractProxyB(1, "通讯B"));
                textViewB.setText(respBeanB.getAge() + " : " + respBeanB.getName());

                for (RespBeanA respBeanA : respBeanB.getRespBeanA()) {
                    Toast.makeText(ProcessInteractActivity.this, respBeanA.getRespString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
