package com.myAndroid.helloworld.process_interact.core;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import com.myAndroid.helloworld.process_interact.ui.proxy.RespBeanB;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 17:42
 * @Description 根据Provider支持跨进程传递数据的机制，设计本套轻量级的进程通讯方案。使用步骤如下：
 * 1. 扩展BaseProcessInteractProvider子类，项目中有几个进程，就扩展几个子类。
 * 2. 扩展BaseProcessInteractProxy子类，每个子类执行在某些特定的通讯操作逻辑。这些子类须严格遵守Parcelable接口的实现规范。
 * 3. 根据每个扩展BaseProcessInteractProxy的子类，新建对应的响应对象类，用以封装通讯操作的返回数据集。这些类必须实现自Parcelable接口。
 * 4. 通讯时，调用interact()方法，将对应通讯代理对象传入，该方法会返回一个响应对象。
 */
public class ProcessInteractUtil {
    static final String PROXY = "processInteractProxy";

    public static <RespBean extends Parcelable> RespBean interact(Context context, IProcessInteractProxy<RespBean> processInteractProxy) {
        Bundle args = new Bundle();
        args.putParcelable(PROXY, processInteractProxy);

        String proxyClassName = processInteractProxy.getClass().getName();

        Bundle respBundle = context.getContentResolver().call(processInteractProxy.getUri(), "", proxyClassName, args);

        if (respBundle == null) {
            return null;
        }

        respBundle.setClassLoader(processInteractProxy.getRespBeanClassLoader());

        Object respBean = respBundle.get(BaseProcessInteractProxy.KEY_RESP_BEAN);

        return (RespBean) respBean;
    }
}
