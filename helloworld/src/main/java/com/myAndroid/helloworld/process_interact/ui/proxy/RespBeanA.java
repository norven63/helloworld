package com.myAndroid.helloworld.process_interact.ui.proxy;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author ZhouXinXing
 * @date 2016年07月06日 11:09
 * @Description
 */
public class RespBeanA implements Parcelable {
    private final String respString;

    public RespBeanA(String respString) {
        this.respString = respString;
    }

    public String getRespString() {
        return respString;
    }

    protected RespBeanA(Parcel in) {
        respString = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(respString);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RespBeanA> CREATOR = new Creator<RespBeanA>() {
        @Override
        public RespBeanA createFromParcel(Parcel in) {
            return new RespBeanA(in);
        }

        @Override
        public RespBeanA[] newArray(int size) {
            return new RespBeanA[size];
        }
    };
}
