package com.myAndroid.helloworld.process_interact.core;

import android.net.Uri;
import android.os.Parcelable;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 17:44
 * @Description 进程通讯代理接口
 */
public interface IProcessInteractProxy<RespBean extends Parcelable> extends Parcelable {
    /**
     * 获取所在工作进程对应的Provider的Uri
     */
    Uri getUri();

    /**
     * 获取数据响应对象的ClassLoader，在使用Bundle传递数据时用到
     */
    ClassLoader getRespBeanClassLoader();

    /**
     * 执行通讯工作
     *
     * @return 数据响应对象
     */
    RespBean interact();
}
