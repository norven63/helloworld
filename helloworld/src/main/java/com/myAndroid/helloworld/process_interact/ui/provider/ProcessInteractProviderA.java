package com.myAndroid.helloworld.process_interact.ui.provider;

import android.net.Uri;

import com.myAndroid.helloworld.process_interact.core.BaseProcessInteractProvider;

/**
 * 负责进程A的通讯
 */
public class ProcessInteractProviderA extends BaseProcessInteractProvider {
    public static final String AUTHORITY = "process.a";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + ProcessInteractProviderA.class.getCanonicalName());

    public ProcessInteractProviderA() {
    }
}
