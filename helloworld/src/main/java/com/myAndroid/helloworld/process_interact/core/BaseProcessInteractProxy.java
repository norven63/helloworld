package com.myAndroid.helloworld.process_interact.core;

import android.os.Parcelable;

/**
 * @author ZhouXinXing
 * @date 2016年07月04日 17:42
 * @Description
 */
public abstract class BaseProcessInteractProxy<RespBean extends Parcelable> implements IProcessInteractProxy<RespBean> {
    public static final String KEY_RESP_BEAN = "key_resp_bean";
}
