package com.myAndroid.helloworld.process_interact.ui.provider;

import android.net.Uri;

import com.myAndroid.helloworld.process_interact.core.BaseProcessInteractProvider;

/**
 * 负责进程B的通讯
 */
public class ProcessInteractProviderB extends BaseProcessInteractProvider {
    public static final String AUTHORITY = "b.process.b";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + ProcessInteractProviderB.class.getCanonicalName());

    public ProcessInteractProviderB() {
    }
}
