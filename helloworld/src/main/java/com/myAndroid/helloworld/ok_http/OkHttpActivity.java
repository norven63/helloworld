package com.myAndroid.helloworld.ok_http;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import com.myAndroid.helloworld.R;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpActivity extends Activity {
    private final static String TAG = "okhttp";

    private final static String URL = "http://publicobject.com/helloworld.txt";

    private final OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10L, TimeUnit.SECONDS).build();
    @Bind(R.id.progress_textView)
    TextView progressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_http);
        ButterKnife.bind(this);

        OkHttpUtils.initClient(client);//初始化三方工具库

        download();

        asyncPost();

        asyncGet();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    syncGet();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 下载文件（三方库包）
     */
    private void download() {
        String destFileDir = Environment.getExternalStorageDirectory().getAbsolutePath();//目标文件路径
        String destFileName = "okhttp-test.apk";
        String downLoadUrl = "http://shouji.360tpcdn.com/150929/dea588214bcccd0b1458f7fdf536f616/com.qihoo.haosou_626.apk";
        OkHttpUtils
                .get()
                .url(downLoadUrl)
                .build()
                .execute(new FileCallBack(destFileDir, destFileName) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.e(TAG, "onError :" + e.getMessage());
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        Log.e(TAG, "onResponse :" + response.getAbsolutePath());
                    }

                    @Override
                    public void inProgress(float progress, long total, int id) {
                        int p = (int) (100 * progress);
                        progressTextView.setText(p + "%");
                    }
                });
    }

    /**
     * 异步GET请求
     */
    private void asyncGet() {
        Request request = new Request.Builder()
                .url(URL)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                /*
                 * 此处非UI线程
                 */
                if (response.isSuccessful()) {
                    Log.e(TAG, "asyncGet.response.code=" + response.code());
                    Log.e(TAG, "asyncGet.response.body=" + response.body().string());
                }
            }
        });

        // call.cancel();//取消请求任务
    }

    /**
     * 同步GET请求
     */
    private void syncGet() throws IOException {
        Request request = new Request.Builder()
                .url(URL)
                .build();

        Response response = client.newCall(request).execute();

        if (response.isSuccessful()) {
            Log.e(TAG, "syncGet.response.code=" + response.code());
            Log.e(TAG, "syncGet.response.body=" + response.body().string());
        }
    }

    /**
     * 异步POST请求
     */
    private void asyncPost() {
        /*
         * 构建头部参数
         */
        Headers.Builder headersBuilder = new Headers.Builder();
        headersBuilder.add("head-key1", "head-value1");
        headersBuilder.add("head-key2", "head-value2");
        headersBuilder.add("head-key3", "head-value3");
        Headers headers = headersBuilder.build();

        /*
         * 构建表单参数
         */
        MediaType JSON = MediaType.parse("");
        String json = "{}";
        RequestBody formBody = RequestBody.create(JSON, json);

        /*
         * 构建请求
         */
        Request request = new Request.Builder()
                .url(URL)
                .headers(headers)
                .post(formBody)
                .build();

        /*
         * 发起请求
         */
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
            /*
             * 此处非UI线程
             */
                if (response.isSuccessful()) {
                    Log.e(TAG, "asyncPost.response.code=" + response.code());
                    Log.e(TAG, "asyncPost.response.body=" + response.body().string());
                }
            }
        });

        // call.cancel();//取消请求任务
    }

    /**
     * 上传文件
     */
    private void uploadFile() {
        /*
         * 构建表单参数
         */
//        FormEncodingBuilder formBodyBuilder = new FormEncodingBuilder();
//        formBodyBuilder.add("param-key1", "param-value1");
//        formBodyBuilder.add("param-key2", "param-value2");
//        RequestBody formBody = formBodyBuilder.build();

        /*
         * 构建文件表单
         */
        File file = new File(Environment.getExternalStorageDirectory(), "test.mp4");
        String fileKey = "mFile";
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);//这里的MediaType要和服务端一致

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
//                .addPart(formBody)
                .addPart(Headers.of(
                        "Content-Disposition",
                        "form-data; name=\"" + fileKey + "\"; filename =\"" + file.getName() + "\""), fileBody)
                .build();

        Request request = new Request.Builder()
                .url("file_upload_url")
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }
}
